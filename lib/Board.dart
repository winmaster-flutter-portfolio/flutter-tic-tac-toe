import 'dart:async';
import 'package:flutter/material.dart';

class Board extends StatefulWidget {

  @override
  State<StatefulWidget> createState() =>_BoardState();
}

class _BoardState extends State<Board>  with WidgetsBindingObserver{
  var firstUser = 'X';
  var secondUser = 'O';
  var currentUser = 'X';
  var moves = 0;
  var winner;
  bool gameOver = false;
  var iconSize = 50;

  List board = [
    {'name': '', 'color': Colors.black, 'size': 50.0},
    {'name': '', 'color': Colors.black, 'size': 50.0},
    {'name': '', 'color': Colors.black, 'size': 50.0},
    {'name': '', 'color': Colors.black, 'size': 50.0},
    {'name': '', 'color': Colors.black, 'size': 50.0},
    {'name': '', 'color': Colors.black, 'size': 50.0},
    {'name': '', 'color': Colors.black, 'size': 50.0},
    {'name': '', 'color': Colors.black, 'size': 50.0},
    {'name': '', 'color': Colors.black, 'size': 50.0}
  ];

  @override
  void initState(){
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }
  @override
  void dispose(){
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  reset() {
    setState(() {
      firstUser = 'X';
      secondUser = 'O';
      currentUser = 'X';
      moves = 0;
      winner = null;
      gameOver = false;
      board = [
        {'name': '', 'color': Colors.black, 'size': 50.0},
        {'name': '', 'color': Colors.black, 'size': 50.0},
        {'name': '', 'color': Colors.black, 'size': 50.0},
        {'name': '', 'color': Colors.black, 'size': 50.0},
        {'name': '', 'color': Colors.black, 'size': 50.0},
        {'name': '', 'color': Colors.black, 'size': 50.0},
        {'name': '', 'color': Colors.black, 'size': 50.0},
        {'name': '', 'color': Colors.black, 'size': 50.0},
        {'name': '', 'color': Colors.black, 'size': 50.0}
      ];
    });
  }

@override
Widget build(BuildContext context) {
  final boxSize = 50.0;
  return Center(
      child:
          Column(
            children: <Widget> [
              Container(
              color: Colors.white70,
              width: MediaQuery.of(context).size.width/2,
              height: MediaQuery.of(context).size.height/2,
              child: GridView.count(
                crossAxisCount: 3,
                crossAxisSpacing: 1,
                children: List.generate(9, (index) {
                  return InkWell(
                   customBorder: CircleBorder(),
                    child: Container(
                      width: boxSize,
                      height: boxSize,
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20))
                      ),
                      margin: EdgeInsets.all(3),
                      child: Center(
                        child: Text(board[index]["name"],
                            style: TextStyle(
                              color: board[index]["color"],
                              fontWeight: FontWeight.bold,
                              fontSize: board[index]["size"],
                            )),
                      ),
                    ),
                    onTap: () {
                      if (board[index]["name"] == "" && winner == null) {
                        onCellClick(index, context);
                      }
                    },
                  );
                },),
              ),
            ),
              Text(currentUser == 'X' ? 'Player is X' : 'Player is O'),
              MaterialButton(
                  onPressed: () => _changePlayer(),
                  child: Text('Change player'))
            ],
          ),
    );
}

onCellClick(index, context) {
  if (board[index]["name"] == "" && gameOver == false) {
    setState(() {
      board[index]["name"] = currentUser;
      moves = moves + 1;
      winner = checkWinner(moves, context);
      currentUser = (currentUser == firstUser) ? secondUser : firstUser;
    });
  }
}

_changePlayer() {
  setState(() {
    currentUser = currentUser == 'O' ? 'X' : 'O';
  });
}

checkWinner(moves, context) {
  var winningCombo = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 4, 8],
    [2, 4, 6],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8]
  ];
  winningCombo.forEach((combo) {
    if ((board[combo[0]]["name"] != "") &&
        (board[combo[1]]["name"] != "") &&
        (board[combo[2]]["name"] != "") &&
        (board[combo[0]]["name"] == board[combo[1]]["name"]) &&
        (board[combo[1]]["name"] == (board[combo[2]]["name"]))) {
      var tempBoard = board;
      board[combo[0]]["color"] = Colors.black;
      board[combo[0]]["size"] = 65.0;
      board[combo[1]]["color"] = Colors.black;
      board[combo[1]]["size"] = 65.0;
      board[combo[2]]["color"] = Colors.black;
      board[combo[2]]["size"] = 65.0;
      setState(() {
        winner = currentUser;
        gameOver = true;
        board = tempBoard;
        currentUser = (currentUser == firstUser) ? secondUser : firstUser;
      });
      Timer(Duration(milliseconds: 500), () {
        showEndMatchDialog(context, 'Winner is Player $currentUser !!');
      });
    }
  });
  if (gameOver == false && moves == 9) {
    setState(() {
      gameOver = true;
    });
    showEndMatchDialog(context, 'It is a draw match !!');
  }
}

void showEndMatchDialog(context, message) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Game Over",
            style:
            TextStyle(color: Colors.black, fontWeight: FontWeight.w500),
          ),
          content: Text(
            message,
            style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.w400,
                textBaseline: TextBaseline.alphabetic),
          ),
          actions: <Widget>[
            FloatingActionButton(
              child: Text('OK', style: TextStyle(color: Colors.black)),
              onPressed: () {
                Navigator.of(context).pop();
                reset();
              },
            ),
          ],
        );
      });
  }
}