import 'package:flutter/material.dart';
import 'Board.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key, String? title}) : title = title!;
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget? _playWidget;

  void _startNewGame() {
    setState(() {
      var playWidget = new Board();
      _playWidget = playWidget;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget? childWidget = _playWidget;
    if (childWidget == null) {
      childWidget = MaterialButton(
        onPressed: () => this._startNewGame(),
        child: Text(
          'Start new game',
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[childWidget],
        ),
      ),
    );
  }
}