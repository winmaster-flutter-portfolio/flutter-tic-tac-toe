# tictactoe

Tic-Tac-Toe in Flutter.

## Application

:rocket: See it in action [here](https://winmaster-flutter-portfolio.gitlab.io/flutter-tic-tac-toe/)
 on Gitlab page.


![](/screens/screen1.png)
![](/screens/screen2.png)
